from pprint import pprint
import random
import arcade
from threading import Thread
from time import sleep


STARTING_ORGANISM_COUNT = 50
MIN_X = 0
MAX_X = 1200
MIN_Y = 0
MAX_Y = 800
MAX_POSSIBLE_LIFESPAN = 4000
STARTING_ENERGY = 1
MATURITY_AGE = 500
MAX_SPEED = 20
SCREEN_WIDTH = MAX_X
SCREEN_HEIGHT = MAX_Y
OLD_AGE = 3000
MIN_ENERGY_TO_PROCREATE = 14
ENERGY_TO_PROCREATE = 6
RELAX = 5
MAX_TIME_TO_BE_DEAD = 30


organisms = []
ticks = 0


class Dna(object):
    def __init__(self, scavenger, photosynthesis, life_span):
        self.scavenger = scavenger
        self.photosynthesis = photosynthesis
        self.life_span = life_span
        self.is_animal = True
        self.speed = random.random() * MAX_SPEED
        self.color = (0, max(int(random.random() * 255), 50),
                      max(int(random.random() * 255), 50))

    def __str__(self):
        return str(self.__dict__)


class Organism(object):
    def __init__(self, id, x, y, energy, birthdate, dna):
        self.id = id
        self.x = x
        self.y = y
        self.energy = energy
        self.birthdate = birthdate
        self.deathdate = None
        self.dna = dna
        self.paused = False

    @property
    def age(self):
        return ticks - self.birthdate

    @property
    def alive(self):
        return self.deathdate is None

    @property
    def time_since_death(self):
        return ticks - self.deathdate

    @property
    def maturity_quotient(self):
        return min(self.age / MATURITY_AGE, 1.0)

    def die(self):
        self.deathdate = ticks

    def should_die(self):
        return self.age > self.dna.life_span or self.energy <= 0.0

    def stay_in_bounds(self):
        if self.y > MAX_Y:
            self.y = MAX_Y

        if self.x > MAX_X:
            self.x = MAX_X

        if self.y < MIN_Y:
            self.y = MIN_Y

        if self.x < MIN_X:
            self.x = MIN_X

    def make_baby(self):
 #       print('make babys')
        self.energy -= ENERGY_TO_PROCREATE
        organisms.append(Organism(random.random(), self.x,
                                  self.y, STARTING_ENERGY, ticks, self.dna))

    def move(self):
        speed = self.dna.speed
        self.x = self.x + (speed/2.0 - random.random() * speed)
        self.y = self.y + (speed/2.0 - random.random() * speed)
        self.stay_in_bounds()
        self.energy -= self.dna.speed / 100.0 - 0.01

    def execute(self):
        if not self.alive:
            return

        if self.should_die():
            self.die()

 #       print(self.energy)
        if self.energy < RELAX:
            self.paused = True

        if self.energy > RELAX + 1:
            self.paused = False

        if not self.paused:
            self.move()

        if self.age > MATURITY_AGE and self.age < OLD_AGE and self.energy > MIN_ENERGY_TO_PROCREATE:
            self.make_baby()

        if self.dna.photosynthesis == True:
            self.energy += 0.01

    def __repr__(self):
        return str(self.__dict__) + f'age: {self.age} alive: {self.alive} energy: {self.energy}'


def draw_organisms(delta_time):
    update_model()

    point_list = []
    color_list = []

    arcade.start_render()

    shape_list = arcade.ShapeElementList()

    for organism in organisms:
        point_list.append((int(organism.x), int(organism.y),))
        point_list.append((int(organism.x+10), int(organism.y),))
        point_list.append((int(organism.x+10), int(organism.y+10),))
       # point_list.append((organism.x, organism.y + 4))
        if organism.alive:
            organism_color = tuple( color_component + (255 - color_component) * (1 - organism.maturity_quotient) for color_component in organism.dna.color)
        else:
            organism_color = (255 - 255 * organism.time_since_death / MAX_TIME_TO_BE_DEAD, 0, 0)
        for i in range(3):
            color_list.append(organism_color)

    shape = arcade.buffered_draw_commands.create_triangles_filled_with_colors(
        point_list, color_list)
    shape_list.append(shape)
    shape_list.draw()


def update_model():
    global ticks

    for organism in list(organisms):
        organism.execute()
        if not organism.alive and organism.time_since_death > MAX_TIME_TO_BE_DEAD:
            organisms.remove(organism)
       # print(organism)

    ticks += 1
    print(f'ticks: {ticks}, organisms: {len(organisms)}')


def main():
    global ticks

    for i in range(STARTING_ORGANISM_COUNT):
        dna_for_organism = Dna(
            True, True, random.random() * MAX_POSSIBLE_LIFESPAN)
        organism = Organism(i, MAX_X * random.random(),
                            MAX_Y * random.random(), STARTING_ENERGY, ticks, dna_for_organism)
        organisms.append(organism)

    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Aias Life")
    arcade.set_background_color(arcade.color.BLACK)
    arcade.schedule(draw_organisms, 1 / 120)
    arcade.run()


main()
